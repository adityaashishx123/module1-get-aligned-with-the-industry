**Product Name**

Virtual Eye Cricket

**Product Link**

[Link](https://virtualeye.tv/the-sports/virtual-eye-cricket)

**Product Short Description**

This product detects object like ball , it segments the path of tracjectory of object,it detects the cricket player, it estimates the pose of cricket player and it annotates the object by giving each object ID.

**Product is combination of features**

1. Object detection
2. Segmentation
3. Pose Estimation
4. ID Recognition
5. Person Detection

**Product is provided by which company?**

Virtual Eye

-------------------------------------------------------------------------------------------------------------------------------------------------------
**Product Name**

Virtual Eye Golf

**Product Link**

[Link](https://virtualeye.tv/the-sports/virtual-eye-golf/59-golf)

**Product Short Description**

This product detects object like ball , it segments the path of tracjectory of object,it estimates the pose of  player and it annotates the object by giving each object ID.

**Product is combination of features**

1. Object detection
2. Segmentation
3. Pose Estimation
4. ID Recognition


**Product is provided by which company?**

Virtual Eye