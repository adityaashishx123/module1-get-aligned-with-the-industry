**Deep Neural Network**  ![DNN Structure](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/DNN.png)

* A deep neural network is a neural network which consists of more than one hidden layer.Its also called as fully connected layer because each neuron is connected to every other neuron of next layer and sometimes it is also called as multilayer perceptron because it is a an artificial neural network consisting of many perceptrons connected together.*

> Neural network  ![Simple neural net](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/Simple_neural_net.png)

*A neural network  is a network of neurons or nodes.*


> Neuron ![Neuron](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/Neuron.png)

*A neuron or perceptron is a mathematical node which computes activation function by giving input values from previous layer.*


>  Activation function ![activation function](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/activation-function.png)

*An activation function is a mathematical function that computes probabilities to give a decision on which neurons to activate for a particular threshold value for a specific task like classification ,by giving input values from previous layer, weights and bias.*

*Most commonly used activation functions are sigmoid so that all values could be squeezed in the range of 0 to 1  and softmax function which maximizes the probability of the output neuron which has highest value.*

> Weights and bias  ![weights_and_bias](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/Weights_and_bias.png)

*Weights and bias are values which are randomly assigned initially in the range of -1 to 1 which are tuned in the network by applying optimizer and loss functions.*

> Loss function ![loss](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/loss_function.png)

* A loss function is a function that computes the cost of the network by comparing the predicted and labelled values.*

> Optimizer function ![optimizer](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/optimizer.png)

*An optimizer function is a function that computes the performance of the network such that weights and biases get altered which results in decreasing log-likelihood loss function.*

*The most likely used optimizer are Gradient Descent, Stochastic Gradient Descent  in which , the data is shuffled initially and then Gradient Descent is applied to a mini-batch of data so that the performance of the neural network increases.*

> Backpropagation  ![backpropagation](https://gitlab.com/adityaashishx123/module1-get-aligned-with-the-industry/-/blob/master/DNN%20images/backpropagation.jpg)

Backpropagation is an algorithm which is used for the training of deep neural network for the purpose of giving correct outputs.  
